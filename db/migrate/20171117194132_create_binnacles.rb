class CreateBinnacles < ActiveRecord::Migration[5.0]
  def change
    create_table :binnacles do |t|
      t.datetime :datetime_scheduled
      t.string :title
      t.text :description
      t.boolean :favorite

      t.timestamps
    end
  end
end
