require 'test_helper'

class BinnacleControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get binnacle_index_url
    assert_response :success
  end

  test "should get show" do
    get binnacle_show_url
    assert_response :success
  end

  test "should get new" do
    get binnacle_new_url
    assert_response :success
  end

end
