class BinnacleController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def create
    #{"title"=>"pastillas", "description"=>"tiroxina", "date"=>"2017-11-17", "time"=>"03:23", "favorite"=>"true"}
    @model = Binnacle.new(:description => params[:description],
                          :title => params[:title],
                          :favorite => params[:favorite])
    #datetime_scheduled => "#{params[:date]} #{params[:time]}",
    @number_status = 400
    if @model.save
      @number_status = 200
    end
  rescue => e
    rescue_with_handler(e)
  ensure
    render json: {
      status: @number_status
    }.to_json
  end
end
